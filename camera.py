from matrix import Matrix4
from math import pi, cos, sin
from numpy import float32


class Camera:
    def __init__(self, aspect_ratio):
        self.yaw = 0.0  # Angle autour de l'axe Y
        self.pitch = 0.0  # Angle autour de l'axe X
        self.distance = 1.0
        self.center = float32((0, 0, 0))
        self.aspect_ratio = aspect_ratio
        self.view_angle = pi/2
        self.nearz = 0.1
        self.farz = 100

    def get_matrix(self):
        mat = Matrix4()
        mat.translate(-self.center[0], -self.center[1], -self.center[2])
        mat.rotate_y(self.yaw)
        mat.rotate_x(self.pitch)
        mat.translate(0, 0, self.distance)
        mat.proj(self.view_angle, self.aspect_ratio, self.nearz, self.farz)
        return mat

    def set_distance(self, distance):
        self.distance = distance

    def get_distance(self):
        return self.distance

    def add_yaw(self, add):
        self.yaw = (self.yaw + add) % (2 * pi)

    def add_pitch(self, add):
        self.pitch = min(max(-pi/2, self.pitch + add), pi/2)

    def set_center(self, ctr):
        self.center = float32(ctr)

    def get_center(self):
        return self.center

    def get_forward(self):
        return float32([sin(self.yaw) * cos(self.pitch), sin(self.pitch), cos(self.yaw) * cos(self.pitch)])

    def get_position(self):
        return self.center - self.distance * self.get_forward()

    def set_aspect_ratio(self, ar):
        self.aspect_ratio = ar
