from ctypes import c_void_p
from gltflib import GLTF
from PIL import Image
from OpenGL.GL import *
from io import BytesIO
from numpy import float32


class GltfModel:
    def __init__(self, fichier):
        self.gpu_images = []
        self.gpu_buffer = None
        self.gpu_mesh_arrays = []
        self.gltf = GLTF.load(fichier)

        assert len(self.gltf.resources) == 1, "Trop de ressources"
        assert len(self.gltf.model.buffers) == 1, "Trop de tampons"
        assert len(self.gltf.resources[0].data) == self.gltf.model.buffers[0].byteLength, "Taille inconsistante"

        if self.gltf.model.images is not None:
            self.load_images()

        self.load_buffer()
        self.create_vertex_arrays()

    def __del__(self):
        glDeleteTextures(self.gpu_images)
        glDeleteBuffers(1, self.gpu_buffer)
        for arrs in self.gpu_mesh_arrays:
            glDeleteVertexArrays(len(arrs), arrs)

    def load_images(self):

        self.gpu_images = glGenTextures(len(self.gltf.model.images))

        for image_index in range(len(self.gltf.model.images)):

            image = self.gltf.model.images[image_index]

            buffer_view = self.gltf.model.bufferViews[image.bufferView]
            image_data = self.gltf.resources[0].data[buffer_view.byteOffset:
                                                     buffer_view.byteOffset + buffer_view.byteLength]
            img = Image.open(BytesIO(image_data))

            glBindTexture(GL_TEXTURE_2D, self.gpu_images[image_index])

            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, img.width, img.height, 0, GL_RGB, GL_UNSIGNED_BYTE,
                         img.tobytes())

            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)

    def load_buffer(self):
        self.gpu_buffer = glGenBuffers(1)

        glBindBuffer(GL_ARRAY_BUFFER, self.gpu_buffer)
        glBufferData(GL_ARRAY_BUFFER, len(self.gltf.resources[0].data), self.gltf.resources[0].data, GL_STATIC_DRAW)

    def create_vertex_arrays(self):
        for mesh in self.gltf.model.meshes:
            self.gpu_mesh_arrays.append([])
            for primitive in mesh.primitives:

                vertex_array = glGenVertexArrays(1)

                self.gpu_mesh_arrays[-1].append(vertex_array)

                glBindVertexArray(vertex_array)

                acc = self.gltf.model.accessors[primitive.attributes.POSITION]
                view = self.gltf.model.bufferViews[acc.bufferView]

                offset = 0
                if acc.byteOffset is not None:
                    offset += acc.byteOffset
                if view.byteOffset is not None:
                    offset += view.byteOffset

                glEnableVertexAttribArray(0)

                glBindBuffer(GL_ARRAY_BUFFER, self.gpu_buffer)
                glVertexAttribPointer(
                    0,
                    3,
                    acc.componentType,
                    (False if acc.normalized is None else acc.normalized),
                    (0 if view.byteStride is None else view.byteStride),
                    c_void_p(offset)
                )

                acc = self.gltf.model.accessors[primitive.attributes.NORMAL]
                view = self.gltf.model.bufferViews[acc.bufferView]

                offset = 0
                if acc.byteOffset is not None:
                    offset += acc.byteOffset
                if view.byteOffset is not None:
                    offset += view.byteOffset

                glEnableVertexAttribArray(1)
                glVertexAttribPointer(
                    1,
                    3,
                    acc.componentType,
                    (False if acc.normalized is None else acc.normalized),
                    (0 if view.byteStride is None else view.byteStride),
                    c_void_p(offset)
                )

                acc = self.gltf.model.accessors[primitive.attributes.TEXCOORD_0]
                view = self.gltf.model.bufferViews[acc.bufferView]

                offset = 0
                if acc.byteOffset is not None:
                    offset += acc.byteOffset
                if view.byteOffset is not None:
                    offset += view.byteOffset

                glEnableVertexAttribArray(2)
                glVertexAttribPointer(
                    2,
                    2,
                    acc.componentType,
                    (False if acc.normalized is None else acc.normalized),
                    (0 if view.byteStride is None else view.byteStride),
                    c_void_p(offset)
                )

                glDisableVertexAttribArray(0)
                glDisableVertexAttribArray(1)
                glDisableVertexAttribArray(2)

                glBindVertexArray(0)

    def render(self, mesh_index, pbr_prog):
        mesh = self.gltf.model.meshes[mesh_index]

        for prim_index in range(len(mesh.primitives)):
            glBindVertexArray(self.gpu_mesh_arrays[mesh_index][prim_index])

            glEnableVertexAttribArray(0)
            glEnableVertexAttribArray(1)
            glEnableVertexAttribArray(2)

            primitive = mesh.primitives[prim_index]

            material_root = self.gltf.model.materials[primitive.material]

            material_pbr = material_root.pbrMetallicRoughness

            if material_pbr.baseColorTexture is not None:
                glUniform1i(pbr_prog.get_uniform_location("u_textured"), 1)
                glActiveTexture(GL_TEXTURE0)
                glBindTexture(GL_TEXTURE_2D, self.gpu_images[material_pbr.baseColorTexture.index])
            else:
                glUniform1i(pbr_prog.get_uniform_location("u_textured"), 0)

            if material_root.normalTexture is not None:
                glUniform1i(pbr_prog.get_uniform_location("u_normal_textured"), 1)
                glActiveTexture(GL_TEXTURE1)
                glBindTexture(GL_TEXTURE_2D, self.gpu_images[material_root.normalTexture.index])
            else:
                glUniform1i(pbr_prog.get_uniform_location("u_normal_textured"), 0)

            glUniform1f(pbr_prog.get_uniform_location("u_metallic"),
                        float32(1 if material_pbr.metallicFactor is None else material_pbr.metallicFactor))
            glUniform1f(pbr_prog.get_uniform_location("u_roughness"),
                        float32(1 if material_pbr.roughnessFactor is None else material_pbr.roughnessFactor))

            glUniform4fv(pbr_prog.get_uniform_location("u_color"), 1, (float32(material_pbr.baseColorFactor)
                                                                       if material_pbr.baseColorFactor is not None else
                                                                       float32([1.0, 1.0, 1.0, 1.0])))

            offset = 0

            ind_acc = self.gltf.model.accessors[primitive.indices]
            ind_view = self.gltf.model.bufferViews[ind_acc.bufferView]

            if ind_acc.byteOffset is not None:
                offset += ind_acc.byteOffset
            if ind_view.byteOffset is not None:
                offset += ind_view.byteOffset

            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, self.gpu_buffer)
            glDrawElements(primitive.mode if primitive.mode is not None else GL_TRIANGLES,
                           ind_acc.count, ind_acc.componentType, c_void_p(offset))

            glDisableVertexAttribArray(0)
            glDisableVertexAttribArray(1)
            glDisableVertexAttribArray(2)

            glBindVertexArray(0)

    def render_shadow(self, mesh_index):
        mesh = self.gltf.model.meshes[mesh_index]
        for primitive_index in range(len(mesh.primitives)):
            primitive = mesh.primitives[primitive_index]

            glBindVertexArray(self.gpu_mesh_arrays[mesh_index][primitive_index])

            glEnableVertexAttribArray(0)

            offset = 0

            ind_acc = self.gltf.model.accessors[primitive.indices]
            ind_view = self.gltf.model.bufferViews[ind_acc.bufferView]

            if ind_acc.byteOffset is not None:
                offset += ind_acc.byteOffset
            if ind_view.byteOffset is not None:
                offset += ind_view.byteOffset

            assert primitive.mode == GL_TRIANGLES or primitive.mode is None

            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, self.gpu_buffer)
            glDrawElements(GL_TRIANGLES, ind_acc.count, ind_acc.componentType, c_void_p(offset))

            glDisableVertexAttribArray(0)

            glBindVertexArray(0)
