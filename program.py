import pygame
import shader

from camera import Camera
from gltf_model import GltfModel
from numpy import float32
from shadowing import *


class Program:
    table_model_path = "objs/billard_3_10.glb"

    def __init__(self):
        self.w = 1280
        self.h = 720
        self.light_pos = float32((0, 2, 0))
        self.shadow_far_z = 100

        self.model_table = None

        self.pbr_prog = None
        self.shadow_cube_prog = None

        self.cam = None

        self.init_pygame()
        self.load_model()
        self.load_shaders()
        self.create_cam()

        self.keep_running = True
        self.zoom_intensity = 1.1

        self.s_cube = CubeShadowGen(self.light_pos)

        glClearColor(0.0, 0.0, 0.0, 0.0)
        glEnable(GL_DEPTH_TEST)
        glDepthFunc(GL_LESS)

        glEnable(GL_CULL_FACE)
        glFrontFace(GL_CW)
        glCullFace(GL_BACK)

    def init_pygame(self):
        pygame.display.set_mode((self.w, self.h), pygame.OPENGL | pygame.DOUBLEBUF | pygame.RESIZABLE | pygame.NOFRAME,
                                vsync=1)
        pygame.display.set_caption("Billard")

        pygame.event.set_grab(True)
        pygame.mouse.set_visible(False)
        pygame.key.set_repeat()

    def load_model(self):
        self.model_table = GltfModel(Program.table_model_path)

    def load_shaders(self):
        self.pbr_prog = shader.Program("shaders/pbr.vert", "shaders/pbr.frag")
        self.shadow_cube_prog = shader.Program("shaders/shadow_cube.vert", "shaders/shadow_cube.frag")

    def create_cam(self):

        self.cam = Camera(self.w / self.h)
        self.cam.set_distance(4)
        self.cam.set_center((0.0, 1.0, 0.0))

    def render(self):

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

        glViewport(0, 0, self.w, self.h)

        glUseProgram(self.pbr_prog.get_id())

        matrix = self.cam.get_matrix()

        glUniformMatrix4fv(self.pbr_prog.get_uniform_location("u_transform"), 1, GL_FALSE, matrix.data)

        glUniform1i(self.pbr_prog.get_uniform_location("u_texture"), 0)
        glUniform1i(self.pbr_prog.get_uniform_location("u_normal_texture"), 1)
        glUniform3fv(self.pbr_prog.get_uniform_location("u_eye_pos"), 1, self.cam.get_position())
        glUniform3fv(self.pbr_prog.get_uniform_location("u_light_pos"), 1, self.light_pos)
        glUniform1f(self.pbr_prog.get_uniform_location("u_far_z"), self.shadow_far_z)

        glUniform1i(self.pbr_prog.get_uniform_location("u_shadow_cube"), 2)
        glActiveTexture(GL_TEXTURE2)
        glBindTexture(GL_TEXTURE_CUBE_MAP, self.s_cube.get_shadow_texture())

        self.model_table.render(0, self.pbr_prog)

        pygame.display.flip()

    def render_shadow_cube(self):
        glDisable(GL_CULL_FACE)

        glUseProgram(self.shadow_cube_prog.get_id())

        glUniform1f(self.shadow_cube_prog.get_uniform_location("u_far_z"), self.shadow_far_z)
        glUniform3fv(self.shadow_cube_prog.get_uniform_location("u_light_pos"), 1, self.light_pos)

        for side in range(6):
            self.s_cube.bind_framebuffer(side)

            glClear(GL_DEPTH_BUFFER_BIT)

            glUniformMatrix4fv(self.shadow_cube_prog.get_uniform_location("u_transform"), 1, GL_FALSE,
                               self.s_cube.get_matrix(side).data)
            self.model_table.render_shadow(0)

        glBindFramebuffer(GL_FRAMEBUFFER, 0)

        glEnable(GL_CULL_FACE)

    def run(self):

        self.render_shadow_cube()

        while self.keep_running:
            self.render()

            event = pygame.event.poll()

            while event.type != pygame.NOEVENT:

                if event.type == pygame.QUIT:
                    self.keep_running = False

                elif event.type == pygame.MOUSEMOTION:
                    self.cam.add_yaw(event.rel[0] * pi / 180)
                    self.cam.add_pitch(- event.rel[1] * pi / 180)

                elif event.type == pygame.MOUSEBUTTONDOWN:
                    if event.button == 4:
                        self.cam.set_distance(self.cam.get_distance() / self.zoom_intensity)
                    elif event.button == 5:
                        self.cam.set_distance(self.cam.get_distance() * self.zoom_intensity)

                elif event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_F11:
                        self.toggle_fullscreen()
                    elif event.key == pygame.K_F3:
                        pygame.event.set_grab(not pygame.event.get_grab())
                        pygame.mouse.set_visible(not pygame.mouse.get_visible())

                elif event.type == pygame.WINDOWRESIZED:
                    self.update_size()

                event = pygame.event.poll()

    def toggle_fullscreen(self):

        pygame.display.set_mode((0, 0), pygame.OPENGL | pygame.DOUBLEBUF | pygame.RESIZABLE, vsync=1)

        self.update_size()

    def update_size(self):
        (self.w, self.h) = pygame.display.get_window_size()
        print(self.w, self.h)
        glViewport(0, 0, self.w, self.h)
        self.cam.set_aspect_ratio(self.w / self.h)
