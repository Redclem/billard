from math import cos, sin, tan
from numpy import float32


class Matrix4:
    def __init__(self, source=None):
        if type(source) == Matrix4:
            self.data = float32(source.data)
        else:
            self.data = float32([
                [1.0, 0.0, 0.0, 0.0],
                [0.0, 1.0, 0.0, 0.0],
                [0.0, 0.0, 1.0, 0.0],
                [0.0, 0.0, 0.0, 1.0]
            ])

    def mul(self, other):
        res = float32([
            [0.0, 0.0, 0.0, 0.0],
            [0.0, 0.0, 0.0, 0.0],
            [0.0, 0.0, 0.0, 0.0],
            [0.0, 0.0, 0.0, 0.0]
        ])

        for col in range(4):
            for row in range(4):
                for term in range(4):
                    res[row][col] += self[term][col] * other[row][term]

        self.data = res

        return self

    def translate(self, x, y, z):
        trans_mat = Matrix4()
        trans_mat[0][3] = x
        trans_mat[1][3] = y
        trans_mat[2][3] = z

        return self.mul(trans_mat)

    def scale(self, x, y, z):
        scale_mat = Matrix4()
        scale_mat[0][0] = x
        scale_mat[1][1] = y
        scale_mat[2][2] = z

        return self.mul(scale_mat)

    def proj(self, view_angle, aspect_ratio, nearz, farz):
        cotangent = 1 / tan(view_angle / 2)

        proj_mat = Matrix4()
        proj_mat[0][0] = cotangent
        proj_mat[1][1] = cotangent * aspect_ratio
        proj_mat[3][2] = 1
        proj_mat[3][3] = 0
        proj_mat[2][2] = 1 + 2 * nearz / farz
        proj_mat[2][3] = -(1 + proj_mat[2][2]) * nearz

        return self.mul(proj_mat)

    def rotate_y(self, angle):
        rot_mat = Matrix4()

        cos_angle = cos(angle)
        sin_angle = sin(angle)

        rot_mat[0][0] = cos_angle
        rot_mat[2][2] = cos_angle
        rot_mat[2][0] = sin_angle
        rot_mat[0][2] = -sin_angle

        return self.mul(rot_mat)

    def rotate_x(self, angle):
        rot_mat = Matrix4()

        cos_angle = cos(angle)
        sin_angle = sin(angle)

        rot_mat[1][1] = cos_angle
        rot_mat[2][2] = cos_angle
        rot_mat[2][1] = sin_angle
        rot_mat[1][2] = -sin_angle

        return self.mul(rot_mat)

    def cubemap(self, side):
        cube_mat = Matrix4()

        if side == 0:
            cube_mat.data = float32([
                [0, 0, -1, 0],
                [0, -1, 0, 0],
                [1, 0, 0, 0],
                [0, 0, 0, 1]
            ])
        elif side == 1:
            cube_mat.data = float32([
                [0, 0, 1, 0],
                [0, -1, 0, 0],
                [-1, 0, 0, 0],
                [0, 0, 0, 1]
            ])
        elif side == 2:
            cube_mat.data = float32([
                [1, 0, 0, 0],
                [0, 0, 1, 0],
                [0, 1, 0, 0],
                [0, 0, 0, 1]
            ])
        elif side == 3:
            cube_mat.data = float32([
                [1, 0, 0, 0],
                [0, 0, -1, 0],
                [0, -1, 0, 0],
                [0, 0, 0, 1]
            ])
        elif side == 4:
            cube_mat.data = float32([
                [1, 0, 0, 0],
                [0, -1, 0, 0],
                [0, 0, 1, 0],
                [0, 0, 0, 1]
            ])
        else:
            cube_mat.data = float32([
                [-1, 0, 0, 0],
                [0, -1, 0, 0],
                [0, 0, -1, 0],
                [0, 0, 0, 1]
            ])

        return self.mul(cube_mat)

    def __getitem__(self, item):
        return self.data[item]

    def __str__(self):
        return str(self.data)

    def __repr__(self):
        return str(self.data)
