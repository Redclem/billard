from program import Program

if __name__ == "__main__":

    prog = Program()
    prog.run()

    del prog
