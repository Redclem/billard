from OpenGL.GL import *
from matrix import Matrix4
from math import pi


class CubeShadowGen:
    shadow_size = 1024

    def __init__(self, light_pos=(0, 0, 0)):
        self.cubemap = -1
        self.framebuffers = []
        self.create_texture()
        self.create_framebuffers()
        self.light_pos = light_pos

    def create_texture(self):
        self.cubemap = glGenTextures(1)
        glBindTexture(GL_TEXTURE_CUBE_MAP, self.cubemap)
        for side in range(6):
            glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + side, 0, GL_DEPTH_COMPONENT, CubeShadowGen.shadow_size,
                         CubeShadowGen.shadow_size, 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0)

        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_NEAREST)
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_NEAREST)
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE)
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE)
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE)

    def set_light_pos(self, light_pos):
        self.light_pos = light_pos

    def get_light_pos(self):
        return self.light_pos

    def get_matrix(self, side):
        return Matrix4().translate(-self.light_pos[0], -self.light_pos[1], -self.light_pos[2])\
            .cubemap(side).proj(pi / 2, 1, 0.1, 100)

    def create_framebuffers(self):
        self.framebuffers = glGenFramebuffers(6)

        for side in range(6):
            glBindFramebuffer(GL_FRAMEBUFFER, self.framebuffers[side])
            glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_CUBE_MAP_POSITIVE_X + side,
                                   self.cubemap, 0)
            glDrawBuffer(GL_NONE)

            assert glCheckFramebufferStatus(GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE, \
                "Echec creation framebuffer cubemap"

        glBindFramebuffer(GL_FRAMEBUFFER, 0)

    def bind_framebuffer(self, side):

        glViewport(0, 0, CubeShadowGen.shadow_size, CubeShadowGen.shadow_size)
        glBindFramebuffer(GL_FRAMEBUFFER, self.framebuffers[side])

    def get_shadow_texture(self):
        return self.cubemap

