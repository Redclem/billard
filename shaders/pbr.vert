#version 450 core

layout(location = 0) in vec3 i_vertex_pos;
layout(location = 1) in vec3 i_normal;
layout(location = 2) in vec2 i_texture_pos;

layout(location = 0) out vec3 o_position;
layout(location = 1) out vec3 o_normal;
layout(location = 2) out vec2 o_texture_pos;

uniform mat4 u_transform;

void main()
{
    gl_Position = vec4(i_vertex_pos, 1.0) * u_transform;
    o_normal = i_normal;
    o_texture_pos = i_texture_pos;
    o_position = i_vertex_pos;
}