#version 450 core

layout(location = 0) in vec3 i_position;
layout(location = 1) in vec3 i_normal;
layout(location = 2) in vec2 i_texture_pos;

layout(location = 0) out vec4 o_color;

uniform vec3 u_eye_pos;
uniform vec3 u_light_pos;

uniform vec4 u_color;
uniform bool u_textured;
uniform bool u_normal_textured;
uniform sampler2D u_texture;
uniform sampler2D u_normal_texture;
uniform float u_roughness;
uniform float u_metallic;
uniform float u_far_z;

uniform samplerCube u_shadow_cube;

vec3 get_normal_from_map()
{
    /* Source:
     * https://github.com/SaschaWillems/Vulkan-glTF-PBR/blob/master/data/shaders/pbr_khr.frag
     * lignes 125 à 141
     */

    vec3 N = normalize(i_normal);
    vec3 T = normalize(dFdx(i_position) * dFdy(i_texture_pos).t - dFdy(i_position) * dFdx(i_texture_pos).t);
    vec3 B = - normalize(cross(N, T));

    return normalize(mat3(T, B, N) * (texture(u_normal_texture, i_texture_pos).rgb * 2.0 - 1.0));
}

vec3 v = normalize(u_eye_pos - i_position);
vec3 l  = normalize(u_light_pos - i_position);
vec3 n = (u_normal_textured ? get_normal_from_map() : normalize(i_normal));
vec3 h = normalize(v + l);

float light_distance = length(u_light_pos - i_position);

float n_dot_h = dot(n, h);
float n_dot_l = dot(n, l);
float n_dot_v = dot(n, v);
float h_dot_v = dot(h, v);
float h_dot_l = dot(h, l);

vec4 base_color = (u_textured ? u_color * texture(u_texture, i_texture_pos) : u_color);

#define pi 3.1415926535897932384626433832795

/* Source de l'implémentation BRDFs
 * https://github.com/KhronosGroup/glTF/tree/master/specification/2.0#appendix-b-brdf-implementation
 */

float specular_brdf(float alpha)
{
    float a_2 = alpha * alpha;
    float one_min_a_2 = 1.0 - a_2;

    float div_D = n_dot_h * n_dot_h * (-one_min_a_2) + 1;

    float D = a_2 / (pi * div_D * div_D);

    float V = 1 /
    ((abs(n_dot_l) + sqrt(a_2 + one_min_a_2 * n_dot_l * n_dot_l))
    * (abs(n_dot_v) + sqrt(a_2 + one_min_a_2 * n_dot_v * n_dot_v)));

    return V * D;
}

/**

vec3 diffuse_brdf(vec3 color)
{
    return color / pi;
}

vec3 conductor_fresnel(vec3 f0, float brdf)
{
    return brdf * (f0 + (1 - f0) * pow(1 - abs(h_dot_v), 5));
}

vec3 fresnel_mix(float ior, vec3 base, vec3 layer)
{
    float f0_root = (1 - ior) / (1 + ior);
    float f0 = f0_root * f0_root;
    return mix(base, layer, f0 + (1 - f0) * pow(1 - abs(h_dot_v), 5));
}

vec3 fresnel_mix_1_5(vec3 base, vec3 layer)
{
    // ior = 1.5 => f0 = 0.04
    return mix(base, layer, 0.04 + 0.96 * pow(1 - abs(h_dot_v), 5));
}

*/

vec3 final_brdf()
{
    vec3 c_diff = mix(base_color.rgb * 0.96, vec3(0), u_metallic);
    vec3 f0 = mix(vec3(0.04), base_color.rgb, u_metallic);

    vec3 F = f0 + (1 - f0) * pow(1 - abs(h_dot_v), 5);

    vec3 f_diffuse = (1 - F) * c_diff / pi;
    vec3 f_specular = F * specular_brdf(u_roughness * u_roughness);

    return f_diffuse + f_specular;
}

void main()
{
    o_color = vec4(0);
    o_color.a = base_color.a;
    o_color.rgb += 0.1 * base_color.rgb;

    if(texture(u_shadow_cube, -l).z * u_far_z > (light_distance - 0.005))
    {
        o_color.rgb += final_brdf() * clamp(n_dot_l, 0.0, 1.0) * 5.0 / light_distance;
    }
}