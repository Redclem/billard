#version 450

layout(location = 0) in vec3 i_position;

layout(location = 0) out vec3 o_position;

uniform mat4 u_transform;

void main()
{
    gl_Position = vec4(i_position, 1.0) * u_transform;
    o_position = i_position;
}