#version 450

layout(location = 0) in vec3 i_position;

layout(location = 0) out float o_depth;

uniform vec3 u_light_pos;
uniform float u_far_z;

void main()
{
    gl_FragDepth = length(i_position - u_light_pos) / u_far_z;
}