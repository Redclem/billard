from OpenGL.GL import *


class Shader:
    def __init__(self, path, stage):
        self.shaderID = glCreateShader(stage)

        with open(path, "r") as shader_file:
            shader_code = shader_file.read()

        result = GL_FALSE
        glShaderSource(self.shaderID, shader_code)
        glCompileShader(self.shaderID)

        if glGetShaderiv(self.shaderID, GL_COMPILE_STATUS) == GL_FALSE:
            raise RuntimeError("Erreur compilation shader : " + glGetShaderInfoLog(self.shaderID).decode())

    def get_id(self):
        return self.shaderID

    def __del__(self):
        glDeleteShader(self.shaderID)


class Program:
    def __init__(self, vertex_name=None, frag_name=None):
        assert frag_name is not None or vertex_name is None, "Deux shaders requis"
        self.programID = glCreateProgram()
        self.locations = {}
        if frag_name is not None:
            self.vertex_shader = Shader(vertex_name, GL_VERTEX_SHADER)
            self.fragment_shader = Shader(frag_name, GL_FRAGMENT_SHADER)
            self.attach_shader(self.vertex_shader)
            self.attach_shader(self.fragment_shader)
            self.link()

    def attach_shader(self, shader):
        glAttachShader(self.programID, shader.get_id())

    def link(self):
        glLinkProgram(self.programID)
        if glGetProgramiv(self.programID, GL_LINK_STATUS) == GL_FALSE:
            raise RuntimeError("Erreur link program : " + glGetProgramInfoLog(self.programID).decode())

        for uniform in range(glGetProgramiv(self.programID, GL_ACTIVE_UNIFORMS)):
            self.locations[glGetActiveUniform(self.programID, uniform)[0].decode()] = uniform

    def get_id(self):
        return self.programID

    def get_uniform_location(self, uniform):
        if uniform in self.locations.keys():
            return self.locations[uniform]
        else:
            return -1

    def __del__(self):
        glDeleteProgram(self.programID)
